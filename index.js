$(document).ready(function() {/*$ appelle jQuery, document est la page HTML, ready signifie : "si la page HTML est chargé, faire ceci" ici, on créé une fonction*/
   $(window).scroll(function() {/*Ceci est une fonction préprogrammé dans JQuery qui permet d'infuler sur le scroll*/
    if($(window).scrollTop() < 1000) {/*Si la fenetre scrolle à partir du haut  : :*/
      $('.animation').css('background-size', 130 + parseInt($(window).scrollTop() / 5) + '%');/*On influe sur la taille du fond, 130est le zoom*/
      $('.animation h1').css('top', 50 + ($(window).scrollTop() * .1) + '%');/*Sur la taille du titre*/
      $('.animation h1').css('opacity', 1 - ($(window).scrollTop() * .003));/*Et sur l'opacity du texte, qui tende vers 0*/
     }
  });
});
